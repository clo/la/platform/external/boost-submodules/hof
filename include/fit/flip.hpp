/*=============================================================================
    Copyright (c) 2015 Paul Fultz II
    flip.h
    Distributed under the Boost Software License, Version 1.0. (See accompanying
    file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
==============================================================================*/

#ifndef FIT_GUARD_FLIP_H
#define FIT_GUARD_FLIP_H

/// flip
/// ====
/// 
/// Description
/// -----------
/// 
/// The `flip` function adaptor swaps the first two parameters.
/// 
/// Synopsis
/// --------
/// 
///     template<class F>
///     flip_adaptor<F> flip(F f);
/// 
/// Semantics
/// ---------
/// 
///     assert(flip(f)(x, y, xs...) == f(y, x, xs...));
/// 
/// Requirements
/// ------------
/// 
/// F must be:
/// 
/// * [BinaryCallable](BinaryCallable)
/// * MoveConstructible
/// 
/// Example
/// -------
/// 
///     #include <fit.hpp>
///     #include <cassert>
/// 
///     int main() {
///         int r = fit::flip(fit::_ - fit::_)(2, 5);
///         assert(r == 3);
///     }
/// 

#include <fit/detail/builder.hpp>
#include <fit/detail/builder/unary.hpp>
#include <fit/reveal.hpp>

namespace fit { namespace detail {

struct flip_adaptor_builder
{
    template<class F>
    struct base
    {
        struct flip_failure
        {
            template<class Failure>
            struct apply
            {
                template<class T, class U, class... Ts>
                struct of
                : Failure::template of<U, T, Ts...>
                {};
            };
        };

        struct failure
        : failure_map<flip_failure, F>
        {};
    };

    struct apply
    {
        template<class F, class T, class U, class... Ts>
        constexpr FIT_SFINAE_RESULT(F&&, id_<U>, id_<T>, id_<Ts>...) 
        operator()(F&& f, T&& x, U&& y, Ts&&... xs) const FIT_SFINAE_RETURNS
        (
            FIT_FORWARD(F)(f)(FIT_FORWARD(U)(y), FIT_FORWARD(T)(x), FIT_FORWARD(Ts)(xs)...)
        );
    };

};

}

FIT_DECLARE_ADAPTOR(flip, detail::unary_adaptor_builder<detail::flip_adaptor_builder>)

} // namespace fit

#endif
